package myProject.entityequals.service.v4;

import myProject.entityequals.domain.v4.GroupEquals4Entity;
import myProject.entityequals.domain.v4.ItemEquals4Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "MERGE INTO entityequals_v4_group(id, group_name) VALUES (1, 'group1')",
        "MERGE INTO entityequals_v4_item(id, group_id, item_name) VALUES (1, 1,'item 1.1')",
        "MERGE INTO entityequals_v4_group(id, group_name) VALUES (2, 'group1u')",
        "MERGE INTO entityequals_v4_item(id, group_id, item_name) VALUES (2, 2,'item 1.1')",
})
public class GroupEquals4ServiceTest {

    @Autowired
    private GroupEquals4Service groupService;

    @Test
    public void save1() {
        GroupEquals4Entity group = new GroupEquals4Entity();
        group.setGroupName("group1");
        ItemEquals4Entity item;
        item = new ItemEquals4Entity();
        item.setItemName("item 1.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    public void save2() {
        GroupEquals4Entity group = new GroupEquals4Entity();
        group.setGroupName("group2");
        ItemEquals4Entity item;
        item = new ItemEquals4Entity();
        item.setItemName("item 2.1");
        group.getItems().add(item);

        item = new ItemEquals4Entity();
        item.setItemName("item 2.2");
        group.getItems().add(item);
        assertEquals(2, group.getItems().size());
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void getAndSave() {
        GroupEquals4Entity group = groupService.get(1L);
        assertNotNull(group);
        assertEquals("group1", group.getGroupName());
        ItemEquals4Entity item;
        item = new ItemEquals4Entity();
        item.setItemName("item 1.2");
        group.getItems().add(item);
        groupService.save(group);
        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void saveAndCompare() {
        GroupEquals4Entity group = new GroupEquals4Entity();
        group.setGroupName("group4");
        ItemEquals4Entity item1 = new ItemEquals4Entity();
        item1.setItemName("item 4.1");
        item1.setGroup(group);
        group.getItems().add(item1);
        groupService.save(group);

        ItemEquals4Entity item2 = new ItemEquals4Entity();
        item2.setItemName("item 4.1");
        item2.setGroup(group);
        assertEquals(item1, item2);
    }

    @Test
    @Transactional
    public void compare1() {
        ItemEquals4Entity item1 = groupService.get(1L).getItems().get(0);
        ItemEquals4Entity item2 = groupService.get(1L).getItems().get(0);
        assertEquals(item1, item2);
    }

    @Test
    @Transactional
    public void compare2() { // Different items is Equal
        ItemEquals4Entity item1 = groupService.get(1L).getItems().get(0);
        ItemEquals4Entity item2 = groupService.get(2L).getItems().get(0);
        assertNotEquals(item1, item2); //FAIL
    }

    @Test
    @Transactional
    public void setProblem() { //lost object in set
        Set<ItemEquals4Entity> set = new HashSet<>();

        ItemEquals4Entity item = new ItemEquals4Entity();
        item.setItemName("item");
        set.add(item);
        assertEquals(true, set.contains(item));

        item.setItemName("item2");
        assertEquals(true, set.contains(item)); //FAIL
        fail();
    }

}