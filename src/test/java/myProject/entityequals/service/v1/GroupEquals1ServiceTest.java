package myProject.entityequals.service.v1;

import myProject.entityequals.domain.v1.GroupEquals1Entity;
import myProject.entityequals.domain.v1.ItemEquals1Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "MERGE INTO entityequals_v1_group(id, group_name) VALUES (1, 'group1')",
        "MERGE INTO entityequals_v1_item(id, group_id, item_name) VALUES (1, 1,'item 1.1')",
        "MERGE INTO entityequals_v1_group(id, group_name) VALUES (2, 'group1u')",
        "MERGE INTO entityequals_v1_item(id, group_id, item_name) VALUES (2, 2,'item 1.1')",
})
public class GroupEquals1ServiceTest {

    @Autowired
    GroupEquals1Service groupService;

    @Test
    public void save1() {
        GroupEquals1Entity group = new GroupEquals1Entity();
        group.setGroupName("group1");
        ItemEquals1Entity item;
        item = new ItemEquals1Entity();
        item.setItemName("item 1.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    public void save2() {
        GroupEquals1Entity group = new GroupEquals1Entity();
        group.setGroupName("group2");
        ItemEquals1Entity item;
        item = new ItemEquals1Entity();
        item.setItemName("item 2.1");
        group.getItems().add(item);

        item = new ItemEquals1Entity();
        item.setItemName("item 2.2");
        group.getItems().add(item);
        assertEquals(2, group.getItems().size());
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void getAndSave() {
        GroupEquals1Entity group = groupService.get(1L);
        assertNotNull(group);
        assertEquals("group1", group.getGroupName());
        ItemEquals1Entity item;
        item = new ItemEquals1Entity();
        item.setItemName("item 1.2");
        group.getItems().add(item);
        groupService.save(group);
        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void saveAndUpdate() {
        GroupEquals1Entity group = new GroupEquals1Entity();
        group.setGroupName("group3");
        ItemEquals1Entity item;
        item = new ItemEquals1Entity();
        item.setItemName("item 3.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        item = new ItemEquals1Entity();
        item.setItemName("item 3.2");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void compare1() {
        ItemEquals1Entity item1 = groupService.get(1L).getItems().get(0);
        ItemEquals1Entity item2 = groupService.get(1L).getItems().get(0);
        assertEquals(item1, item2);
    }

    @Test
    @Transactional
    public void compare2() {
        ItemEquals1Entity item1 = groupService.get(1L).getItems().get(0);
        ItemEquals1Entity item2 = groupService.get(2L).getItems().get(0);
        assertNotEquals(item1, item2);
    }

}