package myProject.entityequals.service.v2;

import myProject.entityequals.domain.v2.GroupEquals2Entity;
import myProject.entityequals.domain.v2.ItemEquals2Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "MERGE INTO entityequals_v2_group(id, group_name) VALUES (1, 'group1')",
        "MERGE INTO entityequals_v2_item(id, group_id, item_name) VALUES (1, 1,'item 1.1')"
})
public class GroupEquals2ServiceTest {

    @Autowired
    private GroupEquals2Service groupService;

    @Test
    public void save1() {
        GroupEquals2Entity group = new GroupEquals2Entity();
        group.setGroupName("group1");
        ItemEquals2Entity item;
        item = new ItemEquals2Entity();
        item.setItemName("item 1.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    public void save2() {
        GroupEquals2Entity group = new GroupEquals2Entity();
        group.setGroupName("group2");
        ItemEquals2Entity item;
        item = new ItemEquals2Entity();
        item.setItemName("item 2.1");
        group.getItems().add(item);

        item = new ItemEquals2Entity();
        item.setItemName("item 2.2");
        group.getItems().add(item);
        assertEquals(2, group.getItems().size());
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void getAndSave() {
        GroupEquals2Entity group = groupService.get(1L);
        assertNotNull(group);
        assertEquals("group1", group.getGroupName());
        ItemEquals2Entity item;
        item = new ItemEquals2Entity();
        item.setItemName("item 1.2");
        group.getItems().add(item);
        groupService.save(group);
        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

}