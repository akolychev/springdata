package myProject.entityequals.service.v5;

import myProject.entityequals.domain.v5.GroupEquals5Entity;
import myProject.entityequals.domain.v5.ItemEquals5Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "MERGE INTO entityequals_v5_group(id, group_name) VALUES (1, 'group1')",
        "MERGE INTO entityequals_v5_item(id, group_id, item_name) VALUES (1, 1,'item 1.1')"
})
public class GroupEquals5ServiceTest {

    @Autowired
    private GroupEquals5Service groupService;

    @Test
    public void save1() {
        GroupEquals5Entity group = new GroupEquals5Entity();
        group.setGroupName("group1");
        ItemEquals5Entity item;
        item = new ItemEquals5Entity();
        item.setItemName("item 1.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    public void save2() {
        GroupEquals5Entity group = new GroupEquals5Entity();
        group.setGroupName("group2");
        ItemEquals5Entity item;
        item = new ItemEquals5Entity();
        item.setItemName("item 2.1");
        group.getItems().add(item);

        item = new ItemEquals5Entity();
        item.setItemName("item 2.2");
        group.getItems().add(item);
        assertEquals(2, group.getItems().size());
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void getAndSave() {
        GroupEquals5Entity group = groupService.get(1L);
        assertNotNull(group);
        assertEquals("group1", group.getGroupName());
        ItemEquals5Entity item;
        item = new ItemEquals5Entity();
        item.setItemName("item 1.2");
        group.getItems().add(item);
        groupService.save(group);
        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void saveAndUpdate() {
        GroupEquals5Entity group = new GroupEquals5Entity();
        group.setGroupName("group3");
        ItemEquals5Entity item;
        item = new ItemEquals5Entity();
        item.setItemName("item 3.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        item = new ItemEquals5Entity();
        item.setItemName("item 3.2");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void saveAndCompare() { // Compare before and after saving
        GroupEquals5Entity group = new GroupEquals5Entity();
        group.setGroupName("group4");
        ItemEquals5Entity item1 = new ItemEquals5Entity();
        item1.setItemName("item 4.1");
        item1.setGroup(group);
        group.getItems().add(item1);
        groupService.save(group);

        ItemEquals5Entity item2 = new ItemEquals5Entity();
        item2.setItemName("item 4.1");
        item2.setGroup(group);
        assertEquals(item1, item2);//FAIL
    }
}