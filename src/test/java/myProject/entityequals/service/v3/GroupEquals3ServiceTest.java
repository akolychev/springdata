package myProject.entityequals.service.v3;

import myProject.entityequals.domain.v3.GroupEquals3Entity;
import myProject.entityequals.domain.v3.ItemEquals3Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "MERGE INTO entityequals_v3_group(id, group_name) VALUES (1, 'group1')",
        "MERGE INTO entityequals_v3_item(id, group_id, item_name) VALUES (1, 1,'item 1.1')"
})
public class GroupEquals3ServiceTest {

    @Autowired
    private GroupEquals3Service groupService;

    @Test
    public void save1() {
        GroupEquals3Entity group = new GroupEquals3Entity();
        group.setGroupName("group1");
        ItemEquals3Entity item;
        item = new ItemEquals3Entity();
        item.setItemName("item 1.1");
        item.setGroup(group);
        group.getItems().add(item);
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    public void save2() { //save 2 items
        GroupEquals3Entity group = new GroupEquals3Entity();
        group.setGroupName("group2");
        ItemEquals3Entity item;
        item = new ItemEquals3Entity();
        item.setItemName("item 2.1");
        group.getItems().add(item);

        item = new ItemEquals3Entity();
        item.setItemName("item 2.2");
        group.getItems().add(item);
        assertEquals(2, group.getItems().size()); //FAIL
        groupService.save(group);

        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

    @Test
    @Transactional
    public void getAndSave() {
        GroupEquals3Entity group = groupService.get(1L);
        assertNotNull(group);
        assertEquals("group1", group.getGroupName());
        ItemEquals3Entity item;
        item = new ItemEquals3Entity();
        item.setItemName("item 1.2");
        group.getItems().add(item);
        groupService.save(group);
        assertEquals(2, group.getItems().size());
        group.getItems()
                .forEach(x -> assertNotNull(x.getId()));
    }

}