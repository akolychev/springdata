package myProject.serialization.service.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {
        "MERGE INTO serialization_v1_group(id, group_name) VALUES (1, 'group1')",
        "MERGE INTO serialization_v1_item(id, group_id, item_name) VALUES (1, 1,'item 1.1')"
})
@Slf4j
public class GroupSerialization1ServiceTest {

    @Autowired
    private GroupSerialization1Service groupService;

    @Test
    @Order(1)
    public void simpleSerialization() throws JsonProcessingException {
        String ret = groupService.getDto(1L);
        log.info(ret);
        assertNotNull(ret);
    }

    @Test
    @Order(2)
    public void update() throws IOException {
        String dto = "{\"id\":1,\"groupName\":\"group1update\",\"items\":[{\"id\":1,\"itemName\":\"item 1.1update\"}]}";
        groupService.update(dto);
        String ret = groupService.getDto(1L);
        assertEquals(dto, ret); //Fail
    }

}