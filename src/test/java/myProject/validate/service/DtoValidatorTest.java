package myProject.validate.service;

import myProject.validate.dto.MyDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.DataBinder;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DtoValidatorTest {

    @Autowired
    private DtoValidator dtoValidator;

    @Test
    public void test() {
        MyDto myDto = new MyDto();

        final DataBinder dataBinder = new DataBinder(myDto);
        dataBinder.addValidators(dtoValidator);
        dataBinder.validate();

        assertTrue(dataBinder.getBindingResult().hasErrors());

        if (dataBinder.getBindingResult().hasErrors()) {
            System.out.println(dataBinder.getBindingResult().toString());
        }

    }

}