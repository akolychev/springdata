package myProject.validate.controller;

import myProject.validate.dto.MyDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/validate")
public class ValidateController {

    @PostMapping(value = "/stepOne")
    public void stepOne(@RequestBody @Validated(value = {MyDto.ValidationStepOne.class}) MyDto dto) {

    }

    @PostMapping(value = "/stepTwo")
    public void stepTwo(@RequestBody @Validated(value = {MyDto.ValidationStepTwo.class}) MyDto dto) {

    }

    @PostMapping(value = "/stepThree")
    public void stepThree(@RequestBody @Validated(value = {MyDto.ValidationStepThree.class}) MyDto dto) {

    }
}
