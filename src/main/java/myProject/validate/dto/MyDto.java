package myProject.validate.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyDto {

    public interface ValidationStepOne {
        // validation group marker interface
    }

    public interface ValidationStepTwo {
        // validation group marker interface
    }

    public interface ValidationStepThree {
        // validation group marker interface
    }

    private Long id;

    @NotBlank(groups = {ValidationStepOne.class})
    private String field1;

    @NotBlank(groups = {ValidationStepTwo.class})
    private String field2;

    @NotNull(groups = {ValidationStepThree.class})
    @Valid
    private Internal internal;

    @Getter
    @Setter
    public class Internal {

        @NotBlank(groups = {ValidationStepThree.class})
        private String internal1;

        private String internal2;
    }

}
