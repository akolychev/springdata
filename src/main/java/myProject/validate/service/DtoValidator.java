package myProject.validate.service;

import myProject.validate.dto.MyDto;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class DtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return MyDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MyDto p = (MyDto) o;
        if (p.getField1() == null || p.getField1().equals("")) {
            errors.reject("Not valid");
        }
    }
}
