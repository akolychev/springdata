package myProject.serialization.service.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.serialization.domain.v1.Group1Entity;
import myProject.serialization.domain.v1.Item1Entity;
import myProject.serialization.repository.v1.Group1Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@Service
public class GroupSerialization1Service {

    @Autowired
    private Group1Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id).get());
    }

    @Transactional
    public Group1Entity update(String dto) throws IOException {
        Group1Entity group1Entity = new ObjectMapper().readValue(dto, Group1Entity.class);
        return groupRepository.save(group1Entity);
    }
//
//    @Transactional
//    public void save(Group1Entity group) {
//        groupRepository.save(group);
//    }
//
//    @Transactional
//    public Group1Entity get(Long id){
//        return groupRepository.findById(id).orElse(null);
//    }
//
//    @Transactional
//    public List<Group1Entity> getAll(){
//        List<Group1Entity> all = groupRepository.findAll();
//        all.forEach(x -> x.getItems().forEach(Item1Entity::getItemName));
//        return all;
//    }

}
