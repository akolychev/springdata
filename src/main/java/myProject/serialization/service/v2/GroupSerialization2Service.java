package myProject.serialization.service.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.serialization.domain.v2.Group2Entity;
import myProject.serialization.repository.v2.Group2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;

@Service
public class GroupSerialization2Service {

    @Autowired
    Group2Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id).get());
    }

    @Transactional
    public Group2Entity update(String dto) throws IOException {
        Group2Entity group1Entity = new ObjectMapper().readValue(dto, Group2Entity.class);
        return groupRepository.save(group1Entity);
    }

}
