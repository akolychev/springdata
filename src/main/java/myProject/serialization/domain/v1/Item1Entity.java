package myProject.serialization.domain.v1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="serialization_v1_item")
@Getter
@Setter
public class Item1Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String itemName;

    @ManyToOne
    @JsonIgnore
    private Group1Entity group;

}
