package myProject.serialization.domain.v2;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="serialization_v2_item")
@Getter
@Setter
public class Item2Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String itemName;

    @Column(name = "group_id")
    private Long group_id;

    @ManyToOne
    @JoinColumn(name = "group_id", insertable=false, updatable=false)
    @JsonIgnore
    private Group2Entity group;



}
