package myProject.serialization.repository.v2;

import myProject.serialization.domain.v2.Group2Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Group2Repository extends JpaRepository<Group2Entity, Long> {



}
