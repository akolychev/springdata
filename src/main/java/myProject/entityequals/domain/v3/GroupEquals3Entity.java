package myProject.entityequals.domain.v3;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="entityequals_v3_group")
@Getter
@Setter
public class GroupEquals3Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String groupName;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private Set<ItemEquals3Entity> items = new HashSet<>();

}
