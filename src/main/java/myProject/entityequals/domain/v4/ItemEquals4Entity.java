package myProject.entityequals.domain.v4;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="entityequals_v4_item")
@Getter
@Setter
public class ItemEquals4Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String itemName;

    @ManyToOne
    private GroupEquals4Entity group;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemEquals4Entity that = (ItemEquals4Entity) o;
        return Objects.equals(itemName, that.itemName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(itemName);
    }
}
