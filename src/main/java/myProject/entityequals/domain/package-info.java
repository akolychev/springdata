/**
 * ----
 * v1 - Basic Entity
 * ----
 * v2 - List -> Set
 * ----
 * v3 - Equals and hash code by id
 * ----
 * v4 - Equals and hash code by fields
 * ----
 * v5 - Equals and hash code by all fields
 * ----
 */
package myProject.entityequals.domain;