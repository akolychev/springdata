package myProject.entityequals.domain.v1;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="entityequals_v1_item")
@Getter
@Setter
public class ItemEquals1Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String itemName;

    @ManyToOne
    private GroupEquals1Entity group;

}
