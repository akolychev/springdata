package myProject.entityequals.domain.v2;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="entityequals_v2_group")
@Getter
@Setter
public class GroupEquals2Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String groupName;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private Set<ItemEquals2Entity> items = new HashSet<>();

}
