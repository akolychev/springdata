package myProject.entityequals.domain.v2;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="entityequals_v2_item")
@Getter
@Setter
public class ItemEquals2Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String itemName;

    @ManyToOne
    private GroupEquals2Entity group;
}
