package myProject.entityequals.domain.v5;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="entityequals_v5_group")
@Getter
@Setter
public class GroupEquals5Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String groupName;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private List<ItemEquals5Entity> items = new ArrayList<>();

}
