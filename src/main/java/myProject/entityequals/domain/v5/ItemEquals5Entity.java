package myProject.entityequals.domain.v5;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="entityequals_v5_item")
@Getter
@Setter
public class ItemEquals5Entity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String itemName;

    @ManyToOne
    private GroupEquals5Entity group;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemEquals5Entity that = (ItemEquals5Entity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(itemName, that.itemName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, itemName);
    }
}
