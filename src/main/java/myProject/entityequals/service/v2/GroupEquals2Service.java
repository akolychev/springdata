package myProject.entityequals.service.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.entityequals.domain.v2.GroupEquals2Entity;
import myProject.entityequals.domain.v2.ItemEquals2Entity;
import myProject.entityequals.repository.v2.GroupEquals2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupEquals2Service {

    @Autowired
    GroupEquals2Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id));
    }

    @Transactional
    public void save(GroupEquals2Entity group) {
        groupRepository.save(group);
    }

    @Transactional
    public GroupEquals2Entity get(Long id){
        return groupRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<GroupEquals2Entity> getAll(){
        List<GroupEquals2Entity> all = groupRepository.findAll();
        all.forEach(x -> x.getItems().forEach(ItemEquals2Entity::getItemName));
        return all;
    }

}
