package myProject.entityequals.service.v4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.entityequals.domain.v3.GroupEquals3Entity;
import myProject.entityequals.domain.v4.GroupEquals4Entity;
import myProject.entityequals.domain.v4.ItemEquals4Entity;
import myProject.entityequals.repository.v4.GroupEquals4Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupEquals4Service {

    @Autowired
    GroupEquals4Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id));
    }

    @Transactional
    public void save(GroupEquals4Entity group) {
        groupRepository.save(group);
    }

    @Transactional
    public GroupEquals4Entity get(Long id){
        return groupRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<GroupEquals4Entity> getAll(){
        List<GroupEquals4Entity> all = groupRepository.findAll();
        all.forEach(x -> x.getItems().forEach(ItemEquals4Entity::getItemName));
        return all;
    }

}
