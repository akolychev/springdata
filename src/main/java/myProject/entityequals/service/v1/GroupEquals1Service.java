package myProject.entityequals.service.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.entityequals.domain.v1.GroupEquals1Entity;
import myProject.entityequals.domain.v1.ItemEquals1Entity;
import myProject.entityequals.repository.v1.GroupEquals1Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupEquals1Service {

    @Autowired
    GroupEquals1Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id));
    }

    @Transactional
    public void save(GroupEquals1Entity group) {
        groupRepository.save(group);
    }

    @Transactional
    public GroupEquals1Entity get(Long id){
        return groupRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<GroupEquals1Entity> getAll(){
        List<GroupEquals1Entity> all = groupRepository.findAll();
        all.forEach(x -> x.getItems().forEach(ItemEquals1Entity::getItemName));
        return all;
    }

}
