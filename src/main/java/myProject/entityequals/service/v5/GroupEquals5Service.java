package myProject.entityequals.service.v5;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.entityequals.domain.v5.GroupEquals5Entity;
import myProject.entityequals.domain.v5.ItemEquals5Entity;
import myProject.entityequals.repository.v5.GroupEquals5Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupEquals5Service {

    @Autowired
    GroupEquals5Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id));
    }

    @Transactional
    public void save(GroupEquals5Entity group) {
        groupRepository.save(group);
    }

    @Transactional
    public GroupEquals5Entity get(Long id){
        return groupRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<GroupEquals5Entity> getAll(){
        List<GroupEquals5Entity> all = groupRepository.findAll();
        all.forEach(x -> x.getItems().forEach(ItemEquals5Entity::getItemName));
        return all;
    }

}
