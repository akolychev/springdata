package myProject.entityequals.service.v3;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import myProject.entityequals.domain.v3.GroupEquals3Entity;
import myProject.entityequals.domain.v3.ItemEquals3Entity;
import myProject.entityequals.repository.v3.GroupEquals3Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupEquals3Service {

    @Autowired
    GroupEquals3Repository groupRepository;

    @Transactional
    public String getDto(Long id) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(groupRepository.findById(id));
    }

    @Transactional
    public void save(GroupEquals3Entity group) {
        groupRepository.save(group);
    }

    @Transactional
    public GroupEquals3Entity get(Long id){
        return groupRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<GroupEquals3Entity> getAll(){
        List<GroupEquals3Entity> all = groupRepository.findAll();
        all.forEach(x -> x.getItems().forEach(ItemEquals3Entity::getItemName));
        return all;
    }

}
