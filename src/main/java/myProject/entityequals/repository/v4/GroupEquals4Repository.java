package myProject.entityequals.repository.v4;

import myProject.entityequals.domain.v4.GroupEquals4Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupEquals4Repository extends JpaRepository<GroupEquals4Entity, Long> {


}
