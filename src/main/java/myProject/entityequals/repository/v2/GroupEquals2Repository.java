package myProject.entityequals.repository.v2;

import myProject.entityequals.domain.v2.GroupEquals2Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupEquals2Repository extends JpaRepository<GroupEquals2Entity, Long> {


}
