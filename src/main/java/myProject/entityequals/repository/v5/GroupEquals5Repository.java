package myProject.entityequals.repository.v5;

import myProject.entityequals.domain.v5.GroupEquals5Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupEquals5Repository extends JpaRepository<GroupEquals5Entity, Long> {


}
