package myProject.entityequals.repository.v3;

import myProject.entityequals.domain.v3.GroupEquals3Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupEquals3Repository extends JpaRepository<GroupEquals3Entity, Long> {


}
