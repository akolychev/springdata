package myProject.entityequals.repository.v1;

import myProject.entityequals.domain.v1.GroupEquals1Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupEquals1Repository extends JpaRepository<GroupEquals1Entity, Long> {


}
