/**
 * not write own equals() nor hashCode() methods for entities
 *      https://struberg.wordpress.com/2016/10/15/tostring-equals-and-hashcode-in-jpa-entities/
 *
 * business key that uniquely identifies each entity
 *      https://medium.com/hashcode/hibernate-equals-and-hascode-323c8ec7111
 *
 * compare table
 *      https://developer.jboss.org/wiki/EqualsandHashCode?_sscc=t
 *
 * Don't Let Hibernate Steal Your Identity
 *      https://web.archive.org/web/20170710132916/http://www.onjava.com/pub/a/onjava/2006/09/13/dont-let-hibernate-steal-your-identity.html?page=3
 *
 */